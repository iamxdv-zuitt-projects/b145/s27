// Create a standard server using Node
// 1. Identify the ingredients/components needed in order to perform the task.

// http-> will allow Node JS to establish a connection in order to transfer data using the 'Hyper Text Transfer Protocol'

// require() -> this will allow us to acquire the resources needed to perform the task

let http = require('http'); 

// 2. Create a server using the methods that we will take from the http resources.
// the http module contains the createServer() method that will allow us to create a standard server set up.

//3. Identify the procedure/task that the server will perform once the connection has been established. 
    // identify 
http.createServer( function(request, response) {
    response.end('Welcome newbie to the server');
    }
).listen(3000)

// 4. Select an address /location where the connection will be established/hosted
// 5. Create a response in the terminal to make sure if the server is running successfully.

console.log('Server is running successfully');

//===========================================================================
// ****NOTE: careful when executing this command: npx kill-port <address>****
//===========================================================================

// 6. Initialize a node package manager (NPM) in our project.
// 7. Install your first package /dependency from the NPM that will allow you to make certain task a lot more easier.
    // adjacent skills:
        // -> install & uninstall Nodemon via terminal
            // install nodemon syntax: npm install -g nodemon
            // uninstall nodemon syntax:npm uninstall nodemon
            // install multiple installation:
                        // syntax 1: npm install <list of dependencies> or
                        // syntax 2: npm i <list of dependencies>
    // a. nodemon
    // b. express

    // 8. Create a remote repository to backup the project
//========================================================================    
        // Reminders: the "node_modules" should not included in the files structure that will be saved online.
            // 
            // Repercussion
                //  its going to take a longer time to stage/push your projects online
                // the "node_modules" will cause error upon deployment

            // USE .gitignore module. -> will allow us to modify the components/contents that version control will track
                // from the terminal use syntax: $touch .gitignore 
                    // -> Be sure that terminal is bashed inside the correct path
                // from the .gitignore file type "/node_module". if .gitignore is placed outside where the "node_module" file use correct path instead /node_module.
//======================================================================== 

